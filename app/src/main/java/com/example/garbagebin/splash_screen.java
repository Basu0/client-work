package com.example.garbagebin;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import static maes.tech.intentanim.CustomIntent.customType;

public class splash_screen extends AppCompatActivity {
ProgressBar progressBar;
int mprogresstatus=0;
private Handler handler=new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        progressBar=(ProgressBar)findViewById(R.id.prograssid);
    new Thread(new Runnable() {
      @Override
      public void run() {
       while (mprogresstatus<100){
           mprogresstatus ++;
           android.os.SystemClock.sleep(50);
           handler.post(new Runnable() {
               @Override
               public void run() {
               progressBar.setProgress(mprogresstatus);
               }
           });
       }
       handler.post(new Runnable() {
           @Override
           public void run() {
               startActivity(new Intent(splash_screen.this,Home_page.class));
               customType(splash_screen.this,"up-to-bottom");
               finish();

           }
       });

      }
     }).start();

   }
   }
